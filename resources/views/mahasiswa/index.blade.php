@extends('layouts/main')
@section('title' ,'Daftar Mahasiswa')

@section('container')
<div class="container">
<div class="row">
<div class="col-10">
<div class="mt-3">
<h1>Daftar mahasiswa</h1>

<table class="table">
  <thead class="table-dark">
  <tr>
    <th scope="col">#</th>
        <th scope="col">Nama</th>
        <th scope="col">NIM</th>
        <th scope="col">email</th>
        <th scope="col">jurusan</th>
        <th scope="col">aksi</th>
    </tr>
    
  </thead>
  <tbody>
  @foreach($mahasiswa as $mhs)
  <tr>
  <th scope="row">{{ $loop->iteration }}</th>
            <td>{{ $mhs->Nama }}</td>
            <td>{{ $mhs->NIM }}</td>
            <td>{{ $mhs->email }}</td>
            <td>{{ $mhs->jurusan }}</td>
            <td>
            <a class="btn btn-success" href="">edit</a>
            <a class="btn btn-danger" href="">delete</a>
            
            
            </td>
  
  </tr>
  @endforeach
  </tbody>
</table>
@endsection
    