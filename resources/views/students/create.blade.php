@extends('layouts/main')
@section('title' ,'Form Tambah Data Mahasiswa')

@section('container')
<div class="container">
<div class="row">
<div class="col-8">
<div class="mt-3">
<h1>Form Tambah Data Mahasiswa </h1>

<form method="post" action="/students" >
@csrf
  <div class="mb-3">
    <label for="nama" class="form-label">Nama</label>
    <input type="text" class="form-control" id="nama"  placeholder="Masukkan Nama">
  <div class="mb-3">
    <label for="NIM" class="form-label">NIM</label>
    <input type="text" class="form-control" id="NIM" placeholder="Masukkan NIM">
  </div>
  <div class="mb-3">
    <label for="email" class="form-label">email</label>
    <input type="text" class="form-control" id="email" placeholder="masukkan email">
  </div>
  <div class="mb-3">
    <label for="jurusan" class="form-label">jurusan</label>
    <input type="text" class="form-control" id="jurusan" placeholder="masukkan jurusan kamu">
  </div>
 
  <button type="submit" class="btn btn-primary">Tambahkan data!</button>
</form>
</div>
</div>
</div>
</div>
@endsection
    