@extends('layouts/main')
@section('title' ,'Daftar Mahasiswa')

@section('container')
<div class="container">
<div class="row">
<div class="col-6">
<div class="mt-3">
<h1>Daftar mahasiswa</h1>

<a href="/students/create" class="btn btn-primary my-3">Tambah data</a>

<ul class="list-group">
@foreach($students as $student)
  <li class="list-group-item d-flex justify-content-between align-items-center">
    {{ $student->Nama}}
    <a href="/students/{{ $student->id }} " class="btn btn-primary">detail</a>
  </li>
 @endforeach
</ul>
@endsection
    