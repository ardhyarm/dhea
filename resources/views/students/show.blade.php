@extends('layouts/main')
@section('title' ,'Detail Mahasiswa')

@section('container')
<div class="container">
<div class="row">
<div class="col-6">
<div class="mt-3">
<h1>Daftar mahasiswa</h1>

<div class="card" >
  <div class="card-body">
    <h5 class="card-title">{{ $student->Nama }}</h5>
    <h6 class="card-subtitle mb-2 text-muted">{{ $student->NIM }}</h6>
    <p class="card-text">{{ $student->email }}</p>
    <p class="card-text">{{ $student->jurusan }}</p>
    <button type="submit" class="btn btn-primary">edit</button>
    <button type="submit" class="btn btn-danger">Delete</button>
    <a href="/students" class="card-link">kembali</a>
  </div>
</div>
</div>
</div>
</div>
</div>
@endsection
    